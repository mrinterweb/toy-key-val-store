# Simple DB
Simple DB is a simple memory resident database with transactional functionality.

To run, simply type ```>ruby ruby-cli.rb```
### Available commands:
```SET <key> <value>  // store the value for key
    GET <key>          // return the current value for key
    DELETE <key>       // remove the entry for key
    COUNT <value>      // return the number of keys that have the given value
    BEGIN              // start a transaction
    COMMIT             // commit all transactions
    ROLLBACK           // rollback current transaction
    ROLLBACK_ALL       // rollback all transactions
    HELP               // this list
```
# Future tasks

  - Better separation of view/model
  - Add more (and bettter) tests
