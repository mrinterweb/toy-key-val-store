class SimpleDb
  HELP_TEXT =
  " The commands available are:
    SET <key> <value>  // store the value for key
    GET <key>          // return the current value for key
    DELETE <key>       // remove the entry for key
    COUNT <value>      // return the number of keys that have the given value
    BEGIN              // start a transaction
    COMMIT             // commit all transactions
    ROLLBACK           // rollback current transaction
    ROLLBACK_ALL       // rollback all transactions
    HELP               // this list"

  def initialize
    @primary_store = {}
    @temp_stores = []
    @store = @primary_store
    @in_transation = false
  end

  def SET(key, val)
    @store[key] = val
  end

  def GET(key)
    @store.fetch(key)
  rescue KeyError => _e
    'Key not set.'
  end

  def DELETE(key)
    @store.delete(key)
  end

  def COUNT(value)
    @store.values.count { |v| v == value }
  end

  def BEGIN
    @store = @store.dup
    @temp_stores << @store
    "Transaction (Level/Nest #{transaction_depth}) Begin"
  end

  def COMMIT
    @temp_stores = []
    @primary_store = @store
  end

  def ROLLBACK
    raise 'Transation depth is less than 0' if transaction_depth < 0

    # drop the last store off the stack
    @temp_stores.pop
    @store = @temp_stores.last || @primary_store
    "Transaction rolled back."
  end

  def ROLLBACK_ALL
    send(:ROLLBACK) while transaction_depth > 0
    "Transactions rolled back."
  end

  private
    def method_missing(method, *args, &block)
      "\e[31mSorry, I don't understand.\n\e[0m" + HELP_TEXT
    end

    def transaction_depth
      @temp_stores.length
    end
end

