require 'minitest/autorun'
require_relative '../lib/simple_db'
require 'pry'

class SimpleDbTest < Minitest::Spec
  before do
    @simple_db = SimpleDb.new
  end

  describe "SimpleDb" do
    describe "Simple commands" do
      before do
        @simple_db.SET("1","1")
      end

      it "should SET and GET data" do
        assert_equal "1", @simple_db.GET("1")
      end

      it "should DELETE data" do
        @simple_db.DELETE("1")
        assert_equal "Key not set.", @simple_db.GET("1")
      end

      it "should return COUNT for num keys with value" do
        assert_equal 1, @simple_db.COUNT("1")
        @simple_db.SET("2", "1")
        assert_equal 2, @simple_db.COUNT("1")
        @simple_db.SET("3", "2")
        assert_equal 2, @simple_db.COUNT("1")
      end

      it "should have help text" do
        refute_empty @simple_db.help
      end
    end

    describe "Transactions" do
      before do
        @simple_db.SET("1","1")
      end

      it "should allow transaction BEGIN" do
        message = @simple_db.BEGIN
        assert_equal "Transaction (Level/Nest 1) Begin", message
      end

      it "should allow transaction ROLLBACK" do
        @simple_db.BEGIN
        @simple_db.SET("3", "3")
        message = @simple_db.ROLLBACK
        assert_equal "Transaction rolled back.", message
      end

      it "should back out all transactions" do
        @simple_db.BEGIN
        @simple_db.SET("3", "3")
        @simple_db.BEGIN
        @simple_db.SET("4", "4")
        message = @simple_db.ROLLBACK_ALL
        assert_equal "Transactions rolled back.", message
        assert_equal "Key not set.", @simple_db.GET("3")
      end

      it "should commit changes" do
        @simple_db.BEGIN
        @simple_db.SET("3", "3")
        @simple_db.COMMIT
        assert_equal "3", @simple_db.GET("3")
      end

      it "should commit non rolled-back changes" do
        @simple_db.BEGIN
        @simple_db.SET("3", "3")
        @simple_db.BEGIN
        @simple_db.SET("4", "4")
        @simple_db.ROLLBACK
        @simple_db.COMMIT
        assert_equal "3", @simple_db.GET("3")
        assert_equal "Key not set.", @simple_db.GET("4")
      end
    end
  end
end
