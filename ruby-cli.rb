require_relative "./lib/simple_db"

puts "Starting SimpleDB..."
simple_db = SimpleDb.new
while true do
  print "> "
  input = gets.chomp.split
  method = input[0]

  if method == "EXIT" then break end
  
  begin
    if input[2]
      puts "=> #{simple_db.send(method, input[1], input[2])}"
    elsif input[1]
      puts "=> #{simple_db.send(method, input[1])}"
    else
      puts "=> #{simple_db.send(method)}"
    end
  rescue ArgumentError
    puts "\e[31mThere was a problem with the input arguments.\e[0m"
    puts simple_db.HELP
  end
end
